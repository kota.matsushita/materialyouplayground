package com.glpgs.materialyouplayground

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.color.DynamicColors

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 描画前に呼び出す必要がありそう
        DynamicColors.applyIfAvailable(this)
        setContentView(R.layout.activity_main)
    }
}